# force_sensor_ws

Catkin workspace for working with the force sensors. It is ready to be cloned, compiled and used directly. 
It uses the `force_sensor` package, see [link](https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/force_sensor).

Make sure to clone it with submodules:
```
git clone --recurse-submodules  git@gitlab.inria.fr:auctus-team/people/antunskuric/ros/catkin_workspaces/force_sensor_ws.git
```